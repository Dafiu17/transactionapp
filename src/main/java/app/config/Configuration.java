package app.config;

import io.javalin.Javalin;

public interface Configuration {

    void configure(Javalin app);
}
