package app.config;

import app.controller.AccountController;
import app.controller.AccountControllerImpl;
import app.dao.AccountDAO;
import app.dao.DAO;
import app.entity.Account;
import app.router.AccountRoute;
import app.router.Route;
import app.router.Router;
import app.service.AccountService;
import app.service.AccountServiceImpl;
import app.utils.AccountMapper;
import io.javalin.Javalin;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class SimpleTransactionAppConfiguration implements Configuration {

    public void configure(Javalin app) {

        DAO<Account> accountDAO = new AccountDAO(new ConcurrentHashMap<>());
        AccountService accountService = new AccountServiceImpl(accountDAO);
        AccountMapper accountMapper = new AccountMapper();
        AccountController accountController = new AccountControllerImpl(accountService, accountMapper);

        List<Route> routes = new ArrayList<>();
        routes.add(new AccountRoute(accountController));

        Router router = new Router(app, routes);
        router.bindRoutes();
    }
}
