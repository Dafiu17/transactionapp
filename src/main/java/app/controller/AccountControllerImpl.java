package app.controller;

import app.dto.AccountDTO;
import app.entity.Account;
import app.entity.Transaction;
import app.service.AccountService;
import app.utils.AccountMapper;
import io.javalin.http.Context;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class AccountControllerImpl implements AccountController {

    private final AccountService accountService;
    private final AccountMapper accountMapper;

    public AccountControllerImpl(AccountService accountService, AccountMapper accountMapper) {
        this.accountService = accountService;
        this.accountMapper = accountMapper;
    }

    @Override
    public void getAllAccounts(Context context) {
        List<AccountDTO> accounts = accountService.getAllAccounts()
                .stream()
                .map(accountMapper::fromOrigin)
                .collect(Collectors.toList());
        context.json(accounts);
        context.status(200);
    }

    @Override
    public void getAccount(Context context) {
        String id = context.pathParam("id");
        Account account = accountService.getAccount(id);
        context.json(account);
        context.status(200);
    }

    @Override
    public void createAccount(Context context) {
        AccountDTO accountDTO = context.bodyAsClass(AccountDTO.class);
        Account account = accountMapper.fromDTO(accountDTO);
        accountService.saveAccount(account);
        context.json(accountMapper.fromOrigin(account));
        context.status(201);
    }

    @Override
    public void transfer(Context context) {
        Transaction transaction = context.bodyAsClass(Transaction.class);
        context.json(accountService.transfer(transaction));
        context.status(200);
    }

    @Override
    public void delete(Context context) {
        UUID id = UUID.fromString(context.pathParam(":id"));
        accountService.delete(id);
        context.status(200);
    }
}
