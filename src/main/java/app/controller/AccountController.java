package app.controller;

import io.javalin.http.Context;

public interface AccountController {

    void getAllAccounts(Context context);
    void getAccount(Context context);
    void createAccount(Context context);
    void transfer(Context context);
    void delete(Context context);
}
