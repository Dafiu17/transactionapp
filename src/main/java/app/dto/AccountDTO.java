package app.dto;

import app.entity.Transaction;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.Queue;
import java.util.UUID;

public class AccountDTO {
    private UUID id;
    private BigDecimal balance;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Queue<Transaction> transactions;

    public AccountDTO(UUID id, BigDecimal balance, Queue<Transaction> transactions) {
        this.id = id;
        this.balance = balance;
        this.transactions = transactions;
    }

    public AccountDTO() {
    }

    public UUID getId() {
        return id;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public Queue<Transaction> getTransactions() {
        return transactions;
    }

}
