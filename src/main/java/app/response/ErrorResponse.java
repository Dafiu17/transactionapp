package app.response;

public interface ErrorResponse {
    String WRONG_UUID_MESSAGE = "Wrong UUID format";
    String ACCOUNT_NOT_FOUND_MESSAGE = "Account was not found";
    String ACCOUNT_ALREADY_EXISTS_MESSAGE = "Account already exists";
    String TRANSACTION_FAILED_NOT_ENOUGH_FUNDS = "Transaction failed, not enough funds in the sender account";
    String BALANCE_MUST_BE_HIGHER_THAN_ZERO = "Balance must be higher than 0";
    String AMOUNT_MUST_BE_HIGHER_THAN_ZERO = "Amount must be higher than 0";
    String THE_SAME_ACCOUNTS_NUMBERS = "Cannot transfer money to the same account";
    String ACCOUNT_NOT_EMPTY = "Account's balance is not empty";
}
