package app.router;

import io.javalin.Javalin;

public interface Route {
    void bind(Javalin app);
}
