package app.router;

import app.controller.AccountController;
import io.javalin.Javalin;

import static io.javalin.apibuilder.ApiBuilder.*;

public class AccountRoute implements Route {

    private final AccountController accountController;

    public AccountRoute(AccountController accountController) {
        this.accountController = accountController;
    }

    @Override
    public void bind(Javalin app) {

        app.routes(() -> {
            path("accounts", () -> {
                get(accountController::getAllAccounts);
                post(accountController::createAccount);
                path(":id", () -> {
                    get(accountController::getAccount);
                    delete(accountController::delete);
                });
                path("transfer", () -> {
                    post(accountController::transfer);
                });
            } );
        });
    }
}
