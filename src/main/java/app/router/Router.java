package app.router;

import io.javalin.Javalin;

import java.util.List;

public class Router {

    private final Javalin app;
    private final List<Route> routes;

    public Router(Javalin app, List<Route> routes) {
        this.app = app;
        this.routes = routes;
    }

    public void bindRoutes() {
        routes.forEach(route -> route.bind(app));
    }
}
