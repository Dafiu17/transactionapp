package app.init;

import app.config.Configuration;
import io.javalin.Javalin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AppRunner {

    private Configuration configuration;
    private Javalin app;
    private static final int PORT = 7000;
    private static final Logger LOGGER = LoggerFactory.getLogger(AppRunner.class);

    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    public Javalin start() {
        app = Javalin.create(config -> {
            config.requestLogger((ctx, ms) -> {
                LOGGER.info("TYPE: {} BODY: {} STATUS: {}", ctx.req.getMethod(), ctx.body(), ctx.status());
            }).enableCorsForAllOrigins();
        }).start(PORT);
        this.configuration.configure(app);
        return app;
    }

    public Javalin shutDown() {
        return app.stop();
    }
}
