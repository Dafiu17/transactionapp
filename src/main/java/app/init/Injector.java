package app.init;

import app.config.Configuration;

public class Injector {

    private final AppRunner appRunner;

    public Injector(AppRunner appRunner) {
        this.appRunner = appRunner;
    }

    public Injector inject(Configuration configuration){
        appRunner.setConfiguration(configuration);
        return this;
    }

    public void boot() {
        appRunner.start();
    }

}
