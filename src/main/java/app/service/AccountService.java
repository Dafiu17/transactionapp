package app.service;

import app.entity.Account;
import app.entity.Transaction;

import java.util.Collection;
import java.util.UUID;

public interface AccountService {

    Collection<Account> getAllAccounts();
    Account getAccount(String id);
    void saveAccount(Account account);
    Transaction transfer(Transaction transaction);
    void delete(UUID id);
}
