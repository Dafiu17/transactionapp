package app.service;

import app.dao.DAO;
import app.entity.Account;
import app.entity.Transaction;
import app.response.ErrorResponse;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.ConflictResponse;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.UUID;

public class AccountServiceImpl implements AccountService {

    private final DAO<Account> accountDAO;

    public AccountServiceImpl(DAO<Account> accountDAO) {
        this.accountDAO = accountDAO;
    }

    @Override
    public Collection<Account> getAllAccounts() {
        return accountDAO.getAll();
    }

    @Override
    public Account getAccount(String id) {
        try {
            UUID uuid = UUID.fromString(id);
            return accountDAO.get(uuid);
        } catch (IllegalArgumentException e) {
            throw new BadRequestResponse(ErrorResponse.WRONG_UUID_MESSAGE);
        }
    }

    @Override
    public void saveAccount(Account account) throws ConflictResponse, BadRequestResponse {
        if (isNegativeValue(account.getBalance())) {
            throw new BadRequestResponse(ErrorResponse.BALANCE_MUST_BE_HIGHER_THAN_ZERO);
        }
        accountDAO.save(account);
    }

    @Override
    public Transaction transfer(Transaction transaction) throws BadRequestResponse {
        Account sender = accountDAO.get(transaction.getSenderID());
        Account receiver = accountDAO.get(transaction.getReceiverID());
        if (isNegativeValue(transaction.getAmount())) {
            throw new BadRequestResponse(ErrorResponse.AMOUNT_MUST_BE_HIGHER_THAN_ZERO);
        }
        if (sender.getId().equals(receiver.getId())) {
            throw new BadRequestResponse(ErrorResponse.THE_SAME_ACCOUNTS_NUMBERS);
        }

        return sender.transfer(receiver, transaction);
    }

    @Override
    public void delete(UUID id) {
        accountDAO.delete(id);
    }

    private boolean isNegativeValue(BigDecimal value) {
        return value.compareTo(new BigDecimal(0)) < 0;
    }
}
