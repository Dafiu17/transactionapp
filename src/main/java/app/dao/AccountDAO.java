package app.dao;

import app.entity.Account;
import app.response.ErrorResponse;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.ConflictResponse;
import io.javalin.http.NotFoundResponse;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class AccountDAO implements DAO<Account> {

    private final ConcurrentHashMap<UUID, Account> accounts;

    public AccountDAO(ConcurrentHashMap<UUID, Account> accounts) {
        this.accounts = accounts;
    }

    @Override
    public Account get(UUID id) throws NotFoundResponse {
        Account account = accounts.get(id);
        if (account != null){
            return account;
        } else {
            throw new NotFoundResponse(ErrorResponse.ACCOUNT_NOT_FOUND_MESSAGE);
        }
    }

    @Override
    public Collection<Account> getAll() {
        return accounts.values();
    }

    @Override
    public void save(Account account) throws ConflictResponse {
        if(accounts.containsKey(account.getId())) {
            throw new ConflictResponse(ErrorResponse.ACCOUNT_ALREADY_EXISTS_MESSAGE);
        }
        accounts.put(account.getId(), account);
    }

    @Override
    public void delete(UUID id) throws NotFoundResponse, BadRequestResponse {
        Account account = accounts.get(id);
        if(account == null) {
            throw new NotFoundResponse(ErrorResponse.ACCOUNT_NOT_FOUND_MESSAGE);
        } else if (account.getBalance().compareTo(new BigDecimal(0)) > 0) {
            throw new BadRequestResponse(ErrorResponse.ACCOUNT_NOT_EMPTY);
        }
        accounts.remove(id);
    }
}
