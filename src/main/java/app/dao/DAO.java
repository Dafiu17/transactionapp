package app.dao;

import java.util.Collection;
import java.util.UUID;

public interface DAO<T> {

    T get(UUID id);

    Collection<T> getAll();

    void save(T t);

    void delete(UUID t);
}
