package app.utils;

import app.dto.AccountDTO;
import app.entity.Account;

import java.util.concurrent.ConcurrentLinkedQueue;

public class AccountMapper {

    public AccountDTO fromOrigin(Account account) {
        return new AccountDTO(account.getId(), account.getBalance(), account.getTransactions());
    }

    public Account fromDTO(AccountDTO accountDTO) {
        return new Account(accountDTO.getId(), accountDTO.getBalance(), new ConcurrentLinkedQueue<>());
    }
}
