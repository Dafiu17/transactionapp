package app.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;

public class Transaction {

    private UUID senderID;
    private UUID receiverID;
    private BigDecimal amount;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String date;

    public Transaction(UUID senderID, UUID receiverID, BigDecimal amount) {
        this.senderID = senderID;
        this.receiverID = receiverID;
        this.amount = amount;
    }

    public Transaction() {
    }

    public UUID getSenderID() {
        return senderID;
    }

    public UUID getReceiverID() {
        return receiverID;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setDate(LocalDateTime date, DateTimeFormatter formatter) {
        this.date = date.format(formatter);
    }
}
