package app.entity;

import app.response.ErrorResponse;
import io.javalin.http.NotFoundResponse;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Queue;
import java.util.UUID;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Account {

    private UUID id;
    private BigDecimal balance;
    private Queue<Transaction> transactions;
    private ReadWriteLock lock = new ReentrantReadWriteLock();
    private Lock writeLock = lock.writeLock();
    private String datePattern = "yyyy-MM-dd HH:mm:ss";

    public Account(UUID id, BigDecimal balance, Queue<Transaction> transactions) {
        this.id = id;
        this.balance = balance;
        this.transactions = transactions;
    }

    public synchronized Transaction transfer(Account receiver, Transaction transaction) {
        this.decrement(transaction.getAmount());
        receiver.increment(transaction.getAmount());
        transaction.setDate(LocalDateTime.now(), DateTimeFormatter.ofPattern(datePattern));
        this.addTransaction(transaction);
        receiver.addTransaction(transaction);
        return transaction;
    }

    public void increment(BigDecimal amount) {
        writeLock.lock();
        try {
            balance = balance.add(amount);
        } finally {
            writeLock.unlock();
        }
    }

    public void decrement(BigDecimal amount) {
        writeLock.lock();
        try {
            BigDecimal result = balance.subtract(amount);
            if (result.compareTo(new BigDecimal(0)) < 0) {
                throw new NotFoundResponse(ErrorResponse.TRANSACTION_FAILED_NOT_ENOUGH_FUNDS);
            }
            balance = balance.subtract(amount);
        } finally {
            writeLock.unlock();
        }
    }

    public UUID getId() {
        return id;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public Queue<Transaction> getTransactions() {
        return transactions;
    }

    public void addTransaction(Transaction transaction) {
        transactions.add(transaction);
    }
}
