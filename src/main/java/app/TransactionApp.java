package app;

import app.config.SimpleTransactionAppConfiguration;
import app.init.AppRunner;
import app.init.Injector;

public class TransactionApp {

    public static void main(String[] args) {
        Injector injector = new Injector(new AppRunner());
        injector.inject(new SimpleTransactionAppConfiguration()).boot();
    }
}