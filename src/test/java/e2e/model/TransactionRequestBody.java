package e2e.model;

import java.math.BigDecimal;
import java.util.UUID;

public class TransactionRequestBody {
    private UUID senderID;
    private UUID receiverID;
    private BigDecimal amount;

    public TransactionRequestBody(UUID senderID, UUID receiverID, BigDecimal amount) {
        this.senderID = senderID;
        this.receiverID = receiverID;
        this.amount = amount;
    }

    public TransactionRequestBody(){}

    public UUID getSenderID() {
        return senderID;
    }

    public UUID getReceiverID() {
        return receiverID;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TransactionRequestBody that = (TransactionRequestBody) o;

        if (!senderID.equals(that.senderID)) return false;
        if (!receiverID.equals(that.receiverID)) return false;
        return amount.equals(that.amount);
    }

    @Override
    public int hashCode() {
        int result = senderID.hashCode();
        result = 31 * result + receiverID.hashCode();
        result = 31 * result + amount.hashCode();
        return result;
    }
}
