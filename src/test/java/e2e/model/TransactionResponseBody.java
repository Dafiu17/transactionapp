package e2e.model;

import java.math.BigDecimal;
import java.util.UUID;

public class TransactionResponseBody {

    private UUID senderID;
    private UUID receiverID;
    private BigDecimal amount;
    private String date;

    public TransactionResponseBody(UUID senderID, UUID receiverID, BigDecimal amount, String date) {
        this.senderID = senderID;
        this.receiverID = receiverID;
        this.amount = amount;
        this.date = date;
    }

    public TransactionResponseBody(){}

    public UUID getSenderID() {
        return senderID;
    }

    public UUID getReceiverID() {
        return receiverID;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getDate() {
        return date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TransactionResponseBody that = (TransactionResponseBody) o;

        if (!senderID.equals(that.senderID)) return false;
        if (!receiverID.equals(that.receiverID)) return false;
        if (!amount.equals(that.amount)) return false;
        return date.equals(that.date);
    }

    @Override
    public int hashCode() {
        int result = senderID.hashCode();
        result = 31 * result + receiverID.hashCode();
        result = 31 * result + amount.hashCode();
        result = 31 * result + date.hashCode();
        return result;
    }
}
