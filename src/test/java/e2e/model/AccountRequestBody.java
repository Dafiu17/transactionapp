package e2e.model;

import java.math.BigDecimal;
import java.util.UUID;

public class AccountRequestBody {

    private UUID id;
    private BigDecimal balance;

    public AccountRequestBody(UUID id, BigDecimal balance) {
        this.id = id;
        this.balance = balance;
    }

    public AccountRequestBody(){}

    public UUID getId() {
        return id;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccountRequestBody that = (AccountRequestBody) o;

        if (!id.equals(that.id)) return false;
        return balance.equals(that.balance);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + balance.hashCode();
        return result;
    }
}
