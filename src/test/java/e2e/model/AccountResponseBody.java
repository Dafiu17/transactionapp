package e2e.model;

import app.entity.Transaction;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public class AccountResponseBody {

    private UUID id;
    private BigDecimal balance;
    private List<Transaction> transactions;

    public AccountResponseBody(UUID id, BigDecimal balance, List<Transaction> transactions) {
        this.id = id;
        this.balance = balance;
        this.transactions = transactions;
    }

    public AccountResponseBody(){}

    public UUID getId() {
        return id;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccountResponseBody that = (AccountResponseBody) o;

        if (!id.equals(that.id)) return false;
        if (!balance.equals(that.balance)) return false;
        return transactions.equals(that.transactions);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + balance.hashCode();
        result = 31 * result + transactions.hashCode();
        return result;
    }
}
