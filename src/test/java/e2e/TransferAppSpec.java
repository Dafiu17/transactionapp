package e2e;

import app.config.SimpleTransactionAppConfiguration;
import app.init.AppRunner;
import e2e.model.AccountRequestBody;
import e2e.model.AccountResponseBody;
import e2e.model.TransactionRequestBody;
import e2e.model.TransactionResponseBody;
import e2e.service.TransferAppService;
import kong.unirest.HttpResponse;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TransferAppSpec {
    private static AppRunner appRunner = new AppRunner();
    private TransferAppService transferAppService = new TransferAppService();

    @BeforeClass
    public static void setup() {
        appRunner.setConfiguration(new SimpleTransactionAppConfiguration());
        appRunner.start();
    }


    @Test
    public void shouldReturnListOfAccounts() {
        //GIVEN
        UUID firstId = UUID.fromString("66fda69a-fcb8-4a4d-8b89-bcdf7ebe6c90");
        UUID secondId = UUID.fromString("911465d7-a4aa-4657-bd63-63bad2e5dd34");
        BigDecimal firstBalance = new BigDecimal(100);
        BigDecimal secondBalance = new BigDecimal(0);
        AccountRequestBody senderAccountRequestBody = new AccountRequestBody(firstId, firstBalance);
        AccountRequestBody receiverAccountRequestBody = new AccountRequestBody(secondId, secondBalance);
        transferAppService.createAccount(senderAccountRequestBody);
        transferAppService.createAccount(receiverAccountRequestBody);
        AccountResponseBody firstResponseBody = new AccountResponseBody(firstId, firstBalance, new ArrayList<>());
        AccountResponseBody secondResponseBody = new AccountResponseBody(secondId, secondBalance, new ArrayList<>());

        //WHEN
        HttpResponse<List<AccountResponseBody>> response = transferAppService.getAllAccounts();
        List<AccountResponseBody> accounts = response.getBody();

        //THEN
        assertEquals(200, response.getStatus());
        assertTrue(accounts.contains(firstResponseBody));
        assertTrue(accounts.contains(secondResponseBody));
    }

    @Test
    public void shouldCreateAnAccount() {
        //GIVEN
        UUID accountId = UUID.fromString("f0daad4a-1158-465a-99ec-d02500bdb974");
        BigDecimal balance = new BigDecimal(100);
        AccountRequestBody accountRequestBody = new AccountRequestBody(accountId, balance);

        //WHEN
        HttpResponse<AccountResponseBody> response = transferAppService.createAccount(accountRequestBody);

        //THEN
        AccountResponseBody responseBody = response.getBody();
        assertEquals(201, response.getStatus());
        assertEquals(responseBody.getBalance(), balance);
        assertEquals(responseBody.getId(), accountId);
        assertTrue(responseBody.getTransactions().isEmpty());
    }

    @Test
    public void shouldTransferMoneyBetweenAccounts() {
        //GIVEN
        UUID senderId = UUID.fromString("a1bd37ad-4bf7-4844-a7dd-181bc5565bc8");
        UUID receiverId = UUID.fromString("7c03aba1-1be8-4d55-964d-3283a9577103");
        BigDecimal senderBalance = new BigDecimal(100);
        BigDecimal receiverBalance = new BigDecimal(0);
        AccountRequestBody senderAccountRequestBody = new AccountRequestBody(senderId, senderBalance);
        AccountRequestBody receiverAccountRequestBody = new AccountRequestBody(receiverId, receiverBalance);
        transferAppService.createAccount(senderAccountRequestBody);
        transferAppService.createAccount(receiverAccountRequestBody);
        BigDecimal amount = new BigDecimal(100);
        TransactionRequestBody transactionRequestBody = new TransactionRequestBody(senderId, receiverId, amount);

        //WHEN
        HttpResponse<TransactionResponseBody> response = transferAppService.transfer(transactionRequestBody);

        //THEN
        assertEquals(200, response.getStatus());

        //WHEN
        HttpResponse<AccountResponseBody> senderResponse = transferAppService.getAccount(senderId.toString());
        HttpResponse<AccountResponseBody> receiverResponse = transferAppService.getAccount(receiverId.toString());
        assertEquals(200, senderResponse.getStatus());
        assertEquals(200, receiverResponse.getStatus());
        assertEquals(senderResponse.getBody().getBalance(), new BigDecimal(0));
        assertEquals(receiverResponse.getBody().getBalance(), new BigDecimal(100));
    }

    @Test
    public void shouldReturnAccount() {
        //GIVEN
        String id = "34d44a51-67cd-4918-93f4-54556567e047";
        UUID uuid = UUID.fromString(id);
        BigDecimal balance = new BigDecimal(100);
        AccountRequestBody senderAccountRequestBody = new AccountRequestBody(uuid, balance);
        transferAppService.createAccount(senderAccountRequestBody);
        AccountResponseBody responseBody = new AccountResponseBody(uuid, balance, new ArrayList<>());


        //WHEN
        HttpResponse<AccountResponseBody> response = transferAppService.getAccount(id);
        AccountResponseBody account = response.getBody();

        //THEN
        assertEquals(200, response.getStatus());
        assertEquals(responseBody, account);
    }

    @Test
    public void shouldDeleteAccount() {
        //GIVEN
        String id = "f79383dd-485d-44cb-8fd4-a71c981e862c";
        UUID uuid = UUID.fromString(id);
        BigDecimal balance = new BigDecimal(0);
        AccountRequestBody accountRequestBody = new AccountRequestBody(uuid, balance);
        transferAppService.createAccount(accountRequestBody);

        //WHEN
        HttpResponse<String> response = transferAppService.deleteAccount(id);

        //THEN
        assertEquals(200, response.getStatus());
    }

    @AfterClass
    public static void cleanup() {
        appRunner.shutDown();
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
