package e2e;

import app.config.SimpleTransactionAppConfiguration;
import app.init.AppRunner;
import e2e.model.AccountRequestBody;
import e2e.model.AccountResponseBody;
import e2e.model.TransactionRequestBody;
import e2e.model.TransactionResponseBody;
import e2e.service.TransferAppService;
import kong.unirest.HttpResponse;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.*;
import static org.junit.Assert.assertEquals;

public class ConcurrentSpec {

    private static final Logger LOGGER = LoggerFactory.getLogger(AppRunner.class);
    private static AppRunner appRunner = new AppRunner();
    private TransferAppService transferAppService = new TransferAppService();

    @BeforeClass
    public static void setup() {
        appRunner.setConfiguration(new SimpleTransactionAppConfiguration());
        appRunner.start();
    }

    @Test
    public void shouldTransferMoneyFromOneAccentToAnother() {
        //GIVEN
        String senderUuid = "a1bd37ad-4bf7-4844-a7dd-181bc5565bc8";
        String receiverUuid = "7c03aba1-1be8-4d55-964d-3283a9577103";

        UUID senderId = UUID.fromString(senderUuid);
        UUID receiverId = UUID.fromString(receiverUuid);
        BigDecimal senderBalance = new BigDecimal(1000);
        BigDecimal receiverBalance = new BigDecimal(0);
        AccountRequestBody senderAccountRequestBody = new AccountRequestBody(senderId, senderBalance);
        AccountRequestBody receiverAccountRequestBody = new AccountRequestBody(receiverId, receiverBalance);
        transferAppService.createAccount(senderAccountRequestBody);
        transferAppService.createAccount(receiverAccountRequestBody);
        BigDecimal amount = new BigDecimal(1);
        TransactionRequestBody transactionRequestBody = new TransactionRequestBody(senderId, receiverId, amount);

        int threads = 1000;
        ExecutorService service = Executors.newFixedThreadPool(10);
        CountDownLatch latch = new CountDownLatch(1);
        Collection<Future<HttpResponse<TransactionResponseBody>>> futures = new ArrayList<>(threads);

        //WHEN
        for (int t = 0; t < threads; ++t) {

            futures.add(
                    service.submit(
                            () -> {
                                latch.await();
                                return transferAppService.transfer(transactionRequestBody);
                            }
                    )
            );
        }
        latch.countDown();
        List<HttpResponse<TransactionResponseBody>> transactions = new LinkedList<>();
        for (Future<HttpResponse<TransactionResponseBody>> f : futures) {
            try {
                transactions.add(f.get());
            } catch (InterruptedException | ExecutionException e) {
                LOGGER.debug("Exception occurred during transfer", e);
            }
        }

        //THEN
        AccountResponseBody senderAccount = transferAppService.getAccount(senderUuid).getBody();
        AccountResponseBody receiverAccount = transferAppService.getAccount(receiverUuid).getBody();
        assertEquals(senderAccount.getBalance(), new BigDecimal(0));
        assertEquals(receiverAccount.getBalance(), new BigDecimal(1000));
        assertEquals(1000, transactions.size());
    }

    @Test
    public void shouldTransferMoneyFromTwoAccentToOne() {
        //GIVEN
        String senderUuid1 = "a5c737b2-7ff6-41e9-a43e-5fc6566d42d2";
        String senderUuid2 = "4a7ce57a-a38e-41bf-844c-420198a64efc";
        String receiverUuid = "bfaacb62-017a-4825-8975-79c038827cb8";

        UUID senderId1 = UUID.fromString(senderUuid1);
        UUID senderId2 = UUID.fromString(senderUuid2);
        UUID receiverId = UUID.fromString(receiverUuid);
        BigDecimal senderBalance = new BigDecimal(1000);
        BigDecimal receiverBalance = new BigDecimal(0);
        AccountRequestBody senderAccountRequestBody1 = new AccountRequestBody(senderId1, senderBalance);
        AccountRequestBody senderAccountRequestBody2 = new AccountRequestBody(senderId2, senderBalance);
        AccountRequestBody receiverAccountRequestBody = new AccountRequestBody(receiverId, receiverBalance);
        transferAppService.createAccount(senderAccountRequestBody1);
        transferAppService.createAccount(senderAccountRequestBody2);
        transferAppService.createAccount(receiverAccountRequestBody);
        BigDecimal amount = new BigDecimal(1);
        TransactionRequestBody transactionRequestBody1 = new TransactionRequestBody(senderId1, receiverId, amount);
        TransactionRequestBody transactionRequestBody2 = new TransactionRequestBody(senderId2, receiverId, amount);

        int threads = 1000;
        ExecutorService service = Executors.newFixedThreadPool(10);
        CountDownLatch latch = new CountDownLatch(1);
        Collection<Future<Integer>> futures = new ArrayList<>(threads);

        //WHEN
        for (int t = 0; t < threads; ++t) {

            futures.add(
                    service.submit(
                            () -> {
                                latch.await();
                                transferAppService.transfer(transactionRequestBody1);
                                transferAppService.transfer(transactionRequestBody2);
                                return 1;
                            }
                    )
            );
        }
        latch.countDown();
        List<Integer> transactions = new LinkedList<>();
        for (Future<Integer> f : futures) {
            try {
                transactions.add(f.get());
            } catch (InterruptedException | ExecutionException e) {
                LOGGER.debug("Exception occurred during transfer", e);
            }
        }

        //THEN
        AccountResponseBody senderAccount1 = transferAppService.getAccount(senderUuid1).getBody();
        AccountResponseBody senderAccount2 = transferAppService.getAccount(senderUuid2).getBody();
        AccountResponseBody receiverAccount = transferAppService.getAccount(receiverUuid).getBody();
        assertEquals(senderAccount1.getBalance(), new BigDecimal(0));
        assertEquals(senderAccount2.getBalance(), new BigDecimal(0));
        assertEquals(receiverAccount.getBalance(), new BigDecimal(2000));
        assertEquals(1000, transactions.size());
    }


    @AfterClass
    public static void cleanup() {
        appRunner.shutDown();
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
