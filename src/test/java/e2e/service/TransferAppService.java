package e2e.service;

import e2e.model.AccountRequestBody;
import e2e.model.AccountResponseBody;
import e2e.model.TransactionRequestBody;
import e2e.model.TransactionResponseBody;
import kong.unirest.GenericType;
import kong.unirest.HttpResponse;
import kong.unirest.JacksonObjectMapper;
import kong.unirest.Unirest;

import java.util.List;

public class TransferAppService {

    private static String HOST = "http://localhost:";
    private static String PORT = "7000";
    private static String ACCOUNTS = "/accounts";
    private static String TRANSFER = "/transfer";
    private static String SEPARATOR = "/";

    public TransferAppService() {
        Unirest.config().setObjectMapper(new JacksonObjectMapper());
    }

    public HttpResponse<AccountResponseBody> createAccount(AccountRequestBody accountRequestBody) {
        return Unirest.post(HOST + PORT + ACCOUNTS)
                .header("accept", "application/json")
                .body(accountRequestBody)
                .asObject(AccountResponseBody.class);
    }

    public HttpResponse<TransactionResponseBody> transfer(TransactionRequestBody transactionRequestBody) {
        return Unirest.post(HOST + PORT + ACCOUNTS + TRANSFER)
                .header("accept", "application/json")
                .body(transactionRequestBody)
                .asObject(TransactionResponseBody.class);
    }

    public HttpResponse<List<AccountResponseBody>> getAllAccounts() {
        return Unirest.get(HOST + PORT + ACCOUNTS)
                .header("accept", "application/json")
                .asObject(new GenericType<List<AccountResponseBody>>() {
                });
    }

    public HttpResponse<AccountResponseBody> getAccount(String uuid) {
        return Unirest.get(HOST + PORT + ACCOUNTS + SEPARATOR + uuid)
                .header("accept", "application/json")
                .asObject(AccountResponseBody.class);
    }

    public HttpResponse<String> deleteAccount(String uuid) {
        return Unirest.delete(HOST + PORT + ACCOUNTS + SEPARATOR + uuid)
                .header("accept", "application/json")
                .asString();
    }


}
