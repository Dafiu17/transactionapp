package app.service;

import app.dao.DAO;
import app.entity.Account;
import app.entity.Transaction;
import io.javalin.http.BadRequestResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceImplSpec {

    private String id = "024ebccc-92ca-464d-947d-92650ae6c9ca";
    private String malformedId = "024ebccc-92ca-464d-947";

    @Mock
    private DAO<Account> accountDAO;

    @Mock
    Collection<Account> accounts;

    @Mock
    private Account account;

    @Mock
    private Transaction transaction;

    @Test
    public void shouldReturnAllAccountsTest() {
        //GIVEN
        when(accountDAO.getAll()).thenReturn(accounts);
        AccountServiceImpl accountService = new AccountServiceImpl(accountDAO);

        //WHEN
        Collection<Account> accountsActual = accountService.getAllAccounts();

        //THEN
        assertEquals(accounts, accountsActual);
    }

    @Test
    public void shouldReturnAccountTest() {
        //GIVEN
        UUID uuid = UUID.fromString(id);
        when(accountDAO.get(uuid)).thenReturn(account);
        AccountServiceImpl accountService = new AccountServiceImpl(accountDAO);

        //WHEN
        Account accountActual = accountService.getAccount(id);

        //THEN
        assertEquals(account, accountActual);
    }

    @Test(expected = BadRequestResponse.class)
    public void shouldThrowBadRequestResponseExceptionWhenWrongIdTest() {
        //GIVEN
        AccountServiceImpl accountService = new AccountServiceImpl(accountDAO);

        //WHEN
        accountService.getAccount(malformedId);
    }

    @Test
    public void shouldSaveAccountTest() {
        //GIVEN
        when(account.getBalance()).thenReturn(new BigDecimal(10));
        AccountServiceImpl accountService = new AccountServiceImpl(accountDAO);

        //WHEN
        accountService.saveAccount(account);

        //THEN
        verify(accountDAO, times(1)).save(account);
    }

    @Test(expected = BadRequestResponse.class)
    public void shouldThrowBadRequestResponseExceptionWhenSavingTest() {
        //GIVEN
        when(account.getBalance()).thenReturn(new BigDecimal(-1));
        AccountServiceImpl accountService = new AccountServiceImpl(accountDAO);

        //WHEN
        accountService.saveAccount(account);
    }

    @Test
    public void shouldTransferTransactionTest() {
        //GIVEN
        UUID senderID = UUID.fromString("024ebccc-92ca-464d-947d-92650ae6c9ca");
        UUID receiverID = UUID.fromString("be215e97-069d-45eb-bd8c-2b940a8f02e1");
        Account sender = mock(Account.class);
        Account receiver = mock(Account.class);
        when(transaction.getReceiverID()).thenReturn(receiverID);
        when(transaction.getSenderID()).thenReturn(senderID);
        when(transaction.getAmount()).thenReturn(new BigDecimal(10));
        when(accountDAO.get(senderID)).thenReturn(sender);
        when(accountDAO.get(receiverID)).thenReturn(receiver);
        when(sender.getId()).thenReturn(senderID);
        when(receiver.getId()).thenReturn(receiverID);

        AccountServiceImpl accountService = new AccountServiceImpl(accountDAO);

        //WHEN
        accountService.transfer(transaction);

        //THEN
        verify(accountDAO, times(1)).get(senderID);
        verify(accountDAO, times(1)).get(receiverID);
        verify(sender, times(1)).transfer(receiver, transaction);
    }

    @Test(expected = BadRequestResponse.class)
    public void shouldThrowBadRequestResponseExceptionWhenTransferringTest() {
        //GIVEN
        UUID senderID = UUID.fromString("024ebccc-92ca-464d-947d-92650ae6c9ca");
        UUID receiverID = UUID.fromString("be215e97-069d-45eb-bd8c-2b940a8f02e1");
        Account sender = mock(Account.class);
        Account receiver = mock(Account.class);
        when(transaction.getReceiverID()).thenReturn(receiverID);
        when(transaction.getSenderID()).thenReturn(senderID);
        when(transaction.getAmount()).thenReturn(new BigDecimal(-1));
        when(accountDAO.get(senderID)).thenReturn(sender);
        when(accountDAO.get(receiverID)).thenReturn(receiver);

        AccountServiceImpl accountService = new AccountServiceImpl(accountDAO);

        //WHEN
        accountService.transfer(transaction);

        //THEN
        verify(accountDAO, times(1)).get(senderID);
        verify(accountDAO, times(1)).get(receiverID);
        verify(sender, times(1)).transfer(receiver, transaction);
    }

    @Test(expected = BadRequestResponse.class)
    public void shouldThrowBadRequestResponseExceptionWhenTransferringToSameAccountTest() {
        UUID senderID = UUID.fromString("024ebccc-92ca-464d-947d-92650ae6c9ca");
        Account sender = mock(Account.class);
        when(transaction.getSenderID()).thenReturn(senderID);
        when(transaction.getReceiverID()).thenReturn(senderID);
        when(transaction.getAmount()).thenReturn(new BigDecimal(10));
        when(accountDAO.get(senderID)).thenReturn(sender);
        when(sender.getId()).thenReturn(senderID);


        AccountServiceImpl accountService = new AccountServiceImpl(accountDAO);

        //WHEN
        accountService.transfer(transaction);

        //THEN
        verify(accountDAO, times(0)).get(senderID);
    }

    @Test
    public void shouldDeleteAccountTest() {
        //GIVEN

        UUID uuid = UUID.fromString(id);
        AccountServiceImpl accountService = new AccountServiceImpl(accountDAO);

        //WHEN
         accountService.delete(uuid);

        //THEN
        verify(accountDAO, times(1)).delete(uuid);
    }

}