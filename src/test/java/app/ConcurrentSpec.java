package app;

import app.entity.Account;
import app.entity.Transaction;
import app.init.AppRunner;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.*;

import static org.junit.Assert.assertEquals;

public class ConcurrentSpec {
    private static final Logger LOGGER = LoggerFactory.getLogger(AppRunner.class);

    @Test
    public void shouldTransferMoneyFromOneAccentToAnother() {
        //GIVEN
        UUID senderID = UUID.fromString("50ea2f6d-092b-4b1b-a5c9-619277ce587a");
        UUID receiverID = UUID.fromString("cf389a93-d7b2-440b-b66f-f4a3d70ae365");
        Account sender = new Account(senderID, new BigDecimal(10000), new ConcurrentLinkedQueue<>());
        Account receiver = new Account(receiverID, new BigDecimal(0), new ConcurrentLinkedQueue<>());

        int threads = 10000;
        ExecutorService service = Executors.newFixedThreadPool(10);
        CountDownLatch latch = new CountDownLatch(1);
        Collection<Future<Transaction>> futures = new ArrayList<>(threads);

        //WHEN
        for (int t = 0; t < threads; ++t) {

            futures.add(
                    service.submit(
                            () -> {
                                latch.await();
                                return sender
                                        .transfer(receiver, new Transaction(senderID, receiverID, new BigDecimal(1)));
                            }
                    )
            );
        }
        latch.countDown();
        Set<Transaction> transactions = new HashSet<>();
        for (Future<Transaction> f : futures) {
            try {
                transactions.add(f.get());
            } catch (InterruptedException | ExecutionException e) {
                LOGGER.debug("Exception occurred during transfer", e);
            }
        }

        //THEN
        assertEquals(new BigDecimal(0), sender.getBalance());
        assertEquals(new BigDecimal(10000), receiver.getBalance());
        assertEquals(10000, transactions.size());
    }

    @Test
    public void shouldTransferMoneyFromTwoAccentToOne() {
        //GIVEN
        UUID senderID1 = UUID.fromString("50ea2f6d-092b-4b1b-a5c9-619277ce587a");
        UUID senderID2 = UUID.fromString("9c70e479-7547-4330-8db3-43dc821512e2");
        UUID receiverID = UUID.fromString("cf389a93-d7b2-440b-b66f-f4a3d70ae365");
        Account sender1 = new Account(senderID1, new BigDecimal(10000), new ConcurrentLinkedQueue<>());
        Account sender2 = new Account(senderID2, new BigDecimal(10000), new ConcurrentLinkedQueue<>());
        Account receiver = new Account(receiverID, new BigDecimal(0), new ConcurrentLinkedQueue<>());

        int threads = 10000;
        ExecutorService service = Executors.newFixedThreadPool(10);
        CountDownLatch latch = new CountDownLatch(1);
        Collection<Future<Integer>> futures = new ArrayList<>(threads);

        //WHEN
        for (int t = 0; t < threads; ++t) {

            futures.add(
                    service.submit(
                            () -> {
                                latch.await();
                                sender1.transfer(receiver, new Transaction(senderID1, receiverID, new BigDecimal(1)));
                                sender2.transfer(receiver, new Transaction(senderID2, receiverID, new BigDecimal(1)));
                                return 1;
                            }
                    )
            );
        }
        latch.countDown();
        List<Integer> transactions = new LinkedList<>();
        for (Future<Integer> f : futures) {
            try {
                transactions.add(f.get());
            } catch (InterruptedException | ExecutionException e) {
                LOGGER.debug("Exception occurred during transfer", e);
            }
        }

        //THEN
        assertEquals(new BigDecimal(0), sender1.getBalance());
        assertEquals(new BigDecimal(0), sender2.getBalance());
        assertEquals(new BigDecimal(20000), receiver.getBalance());
        assertEquals(10000, transactions.size());
    }
}
