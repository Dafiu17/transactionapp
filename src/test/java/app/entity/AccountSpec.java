package app.entity;

import io.javalin.http.NotFoundResponse;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.UUID;

import static org.junit.Assert.*;

public class AccountSpec {

    private UUID uuid = UUID.fromString("024ebccc-92ca-464d-947d-92650ae6c9ca");


    @Test
    public void shouldIncrementAccountsBalanceTest() {
        //GIVEN
        BigDecimal amount = new BigDecimal(10);
        Account account = new Account(uuid, new BigDecimal(0), new LinkedList<>());

        //WHEN
        account.increment(amount);

        //THEN
        assertEquals(amount, account.getBalance());

    }

    @Test
    public void shouldDecrementAccountsBalanceTest() {
        //GIVEN
        BigDecimal amount = new BigDecimal(10);
        Account account = new Account(uuid, new BigDecimal(10), new LinkedList<>());

        //WHEN
        account.decrement(amount);

        //THEN
        assertEquals(new BigDecimal(0), account.getBalance());

    }

    @Test(expected = NotFoundResponse.class)
    public void shouldThrowNotFoundResponseExceptionWhenDecrementingAccountsBalanceTest() {
        //GIVEN
        BigDecimal amount = new BigDecimal(10);
        Account account = new Account(uuid, new BigDecimal(0), new LinkedList<>());

        //WHEN
        account.decrement(amount);

        //THEN
        assertEquals(new BigDecimal(0), account.getBalance());

    }

    @Test()
    public void shouldTransferMoneyBetweenAccountsTest() {
        //GIVEN
        UUID senderID = UUID.fromString("024ebccc-92ca-464d-947d-92650ae6c9ca");
        UUID receiverID = UUID.fromString("be215e97-069d-45eb-bd8c-2b940a8f02e1");

        BigDecimal amount = new BigDecimal(10);
        Account sender = new Account(senderID, new BigDecimal(10), new LinkedList<>());
        Account receiver = new Account(receiverID, new BigDecimal(0), new LinkedList<>());
        Transaction transaction = new Transaction(senderID, receiverID, amount);

        //WHEN
        sender.transfer(receiver, transaction);

        //THEN
        assertEquals(new BigDecimal(0), sender.getBalance());
        assertEquals(new BigDecimal(10), receiver.getBalance());
        assertEquals(transaction, sender.getTransactions().peek());
        assertEquals(transaction, receiver.getTransactions().peek());

    }


}