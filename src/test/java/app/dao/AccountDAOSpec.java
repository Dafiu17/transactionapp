package app.dao;

import app.entity.Account;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.ConflictResponse;
import io.javalin.http.NotFoundResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AccountDAOSpec {

    private String uuid = "024ebccc-92ca-464d-947d-92650ae6c9ca";

    @Mock
    private Account account;

    @Mock
    ConcurrentHashMap<UUID, Account> accountsDB;

    @Mock
    Collection<Account> accounts;

    @Test
    public void shouldReturnAccountTest() {
        //GIVEN
        UUID id = UUID.fromString(uuid);
        when(accountsDB.get(id)).thenReturn(account);
        accountsDB.put(id, account);
        AccountDAO accountDAO = new AccountDAO(accountsDB);

        //WHEN
        Account returnedAccount = accountDAO.get(id);

        //THEN
        assertEquals(account, returnedAccount);
    }

    @Test(expected = NotFoundResponse.class)
    public void shouldThrowNotFoundResponseExceptionWhenRetrievingAccountTest() {
        //GIVEN
        UUID id = UUID.fromString(uuid);
        when(accountsDB.get(id)).thenReturn(null);
        accountsDB.put(id, account);
        AccountDAO accountDAO = new AccountDAO(accountsDB);

        //WHEN
        accountDAO.get(id);
    }

    @Test
    public void shouldReturnAccountsCollectionTest() {
        //GIVEN
        UUID id = UUID.fromString(uuid);
        when(accountsDB.values()).thenReturn(accounts);
        AccountDAO accountDAO = new AccountDAO(accountsDB);

        //WHEN
        Collection<Account> returnedAccounts = accountDAO.getAll();

        //THEN
        assertEquals(accounts, returnedAccounts);
    }

    @Test
    public void shouldSaveAccountTest() {
        //GIVEN
        UUID id = UUID.fromString(uuid);
        when(accountsDB.containsKey(id)).thenReturn(false);
        when(account.getId()).thenReturn(id);
        AccountDAO accountDAO = new AccountDAO(accountsDB);

        //WHEN
        accountDAO.save(account);

        //THEN
        verify(accountsDB, times(1)).put(id, account);
    }

    @Test(expected = ConflictResponse.class)
    public void shouldThrowConflictResponseExceptionWhenSavingTest() {
        //GIVEN
        UUID id = UUID.fromString(uuid);
        when(accountsDB.containsKey(id)).thenReturn(true);
        when(account.getId()).thenReturn(id);
        AccountDAO accountDAO = new AccountDAO(accountsDB);

        //WHEN
        accountDAO.save(account);

        //THEN
        verify(accountsDB, times(0)).put(id, account);
    }

    @Test
    public void shouldDeleteAccountTest() {
        //GIVEN
        BigDecimal balance = new BigDecimal(0);
        UUID id = UUID.fromString(uuid);
        when(accountsDB.get(id)).thenReturn(account);
        when(account.getBalance()).thenReturn(balance);
        AccountDAO accountDAO = new AccountDAO(accountsDB);

        //WHEN
        accountDAO.delete(id);

        //THEN
        verify(accountsDB, times(1)).remove(id);
    }

    @Test(expected = NotFoundResponse.class)
    public void shouldThrowConflictResponseExceptionWhenDeletingTest() {
        //GIVEN
        BigDecimal balance = new BigDecimal(10);
        UUID id = UUID.fromString(uuid);
        when(accountsDB.get(id)).thenReturn(null);
        when(account.getBalance()).thenReturn(balance);
        AccountDAO accountDAO = new AccountDAO(accountsDB);

        //WHEN
        accountDAO.delete(id);

        //THEN
        verify(accountsDB, times(0)).remove(id);
    }

    @Test(expected = BadRequestResponse.class)
    public void shouldThrowBadRequestResponseExceptionWhenDeletingTest() {
        //GIVEN
        BigDecimal balance = new BigDecimal(10);
        UUID id = UUID.fromString(uuid);
        when(accountsDB.get(id)).thenReturn(account);
        when(account.getBalance()).thenReturn(balance);
        AccountDAO accountDAO = new AccountDAO(accountsDB);

        //WHEN
        accountDAO.delete(id);

        //THEN
        verify(accountsDB, times(0)).remove(id);
    }

}