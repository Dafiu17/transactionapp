Requirement Java 1.8+, Gradle 5.5.1

To start app run following commands:  
gradle jarWithDependencies  
java -jar build/libs/transfer-app-with-dependencies-1.0-SNAPSHOT.jar 

To run tests execute:  
gradlew clean test

API:

Create Account: POST: http://localhost:7000/accounts
```javascript
{ 
    "id": "UUID"
    "balance": number
}
```

eg.:
```javascript
{
    "id": "dcb87d2c-afc6-4149-b81f-5c9867271b96",
    "balance":200
}
```

Transfer: POST: http://localhost:7000/accounts/transfer
```javascript
{
    
    "senderID": "UUID",
    "receiverID": "UUID",
    "amount": number
}
```

eg.:
```javascript
{
    "senderID": "2df4a64f-9571-4709-a789-6b97013ed57a",
    "receiverID": "2df4a64f-9571-4709-a789-6b97013ed57d",
    "amount": 100
}
```


Get all accounts: GET: http://localhost:7000/accounts  
Get account with id: GET: http://localhost:7000/accounts/id  
Delete account with id: DELETE: http://localhost:7000/accounts/id
